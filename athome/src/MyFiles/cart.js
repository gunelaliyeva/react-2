import React, {Component} from 'react';
import Items from "./items";
class Cart extends Component {
    render() {
        return (
            <div className={"cartcontainer"}>
                <Items type={"cart"} component={"Cartitem"}/>
            </div>
        );
    }
}

export default Cart;
