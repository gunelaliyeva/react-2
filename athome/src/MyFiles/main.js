import React, {Component} from 'react';
import Container from "./container";
import './style.scss'
import {Link,Route} from "react-router-dom";
import Header from "./header";
import Content from "./content";

class Main extends Component {
    render() {
        return (
            <div >
                <div className={"main"}>
                    <Header/>
                    <Content/>
                </div>
            </div>
        );
    }
}

export default Main;