import React from 'react';

const Commit = props => {
    const addLocal=()=>{
        if(props.name==="add")
            localStorage.setItem(props.inf.productNumber+"cart",props.inf.name);
        else if(props.name==="delete")
            localStorage.removeItem(props.inf.productNumber+"cart");
    };
    return (
        <div className={props.class} onClick={()=>{props.click();addLocal()}}>
            <p>{props.name}</p>
        </div>
    );
};

export default Commit;