import React from 'react';
import Container from "./MyFiles/container";
import Main from "./MyFiles/main";
function App() {
    return (
    <div className="App">
        <Main/>
    </div>
  );
}

export default App;

/*
import React, {Component} from 'react';
import {Link,Route} from "react-router-dom";

const user=()=><h1>User</h1>
const login=()=><h1>login</h1>
const register=()=><h1>register</h1>
class App extends Component {
    render() {
        return (
            <>
                <h2>React route is a separate library!</h2>
                <p>We need to download it every time for every project</p>
                <p>download command - npm install react-router react-router-dom --save</p>
                <Link to={'/user'}>Home</Link>
                <Link to={'/login'}>Login</Link>
                <Link to={'/register'}>Register</Link>

                <Route path={'/user'} component={user}/>
                <Route path={'/login'} component={login}/>
                <Route path={'/register'} component={register}/>

            </>
        );
    }
}

export default App;

 */