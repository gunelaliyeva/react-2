import React from 'react';
import './style.scss'
import getSymbolFromCurrency from 'currency-symbol-map'
import Button1 from "./button";
import Button2 from './button2'
const Components = props => {
    return (
        <div className={"component"} onClick={props.set}>
            <img src={props.datas.path} alt="" className={"img"}/>
            <h2 className={"productname"}>{props.datas.name}</h2>
            <h2 className={"productprice"}>{props.datas.price} {getSymbolFromCurrency("AZN")}</h2>
            <div className={"buttons"}>
            <Button1 name={"add to cart"} class={"addtocart"}  click={props.open} />
            <Button2 name={""} class={"addtofavorite"} click={props.datas} color={props.color}/>
            </div>
        </div>
    );
};

export default Components;