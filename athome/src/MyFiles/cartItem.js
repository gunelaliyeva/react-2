import React from 'react';
import './style.scss'
import getSymbolFromCurrency from 'currency-symbol-map'
import Button1 from "./button";
const CartItem = props => {
    return (
        <div className={"component"} onClick={props.set}>
            <img src={props.datas.path} alt="" className={"img"}/>
            <h2 className={"productname"}>{props.datas.name}</h2>
            <h2 className={"productprice"}>{props.datas.price} {getSymbolFromCurrency("AZN")}</h2>
                <Button1 name={"X"} class={"delete"}  click={props.open}/>
        </div>
    );
};

export default CartItem;