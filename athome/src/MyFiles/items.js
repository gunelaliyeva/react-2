import React, {Component} from 'react';
import CartItem from "./cartItem";
import Components from "./components";
import Modal from "./modal";
class Items extends Component {
    state={
        all:[],
        visible:false,
        productId:null
    };
    componentDidMount() {
        fetch("data.json").then(item=>item.json()).then(item=>{
            this.setState({all:item});
        });

    }
    getItems=()=>{
        let objs = [],
            keys = Object.keys(localStorage);
        keys.forEach(item=> item.includes(this.props.type)?this.state.all.find(it=>it.productNumber===parseInt(item)?objs.push(it):null):null);
        return objs;
    };
    openModal=()=>{
        this.setState({visible:true})
    };
    setId=(e)=>{
        this.setState({productId:e});
    };
    closeModal=()=>{
        this.setState({visible:false})
    };
    render() {
        const obj=this.getItems();
        return (
            <div className={"item"}>
                {
                    this.state.visible?<Modal click={this.closeModal} data={this.state.productId} title={"are you sure to delete this product from cart"} name={"delete"}/>:null
                }
                {
                    obj.map((item,index)=>this.props.component==="Cartitem"?<CartItem key={index} datas={item}  set={()=>this.setId(item)} open={this.openModal}/>:
                        <Components key={index} datas={item} open={this.openModal} set={()=>this.setId(item)} color={"#ff9900"}/>)
                }
            </div>
        );
    }
}

export default Items;