import React, {Component} from 'react';
import Components from "./components";
import './style.scss'
import Modal from "./modal";
class Container extends Component {
    state={
        arr:[],
        visible:false,
        productId:null
    };
    componentDidMount() {
        fetch("data.json").then(item=>item.json()).then(item=>{
            this.setState({arr:item});
        });
    };
    closeModal=()=>{
        this.setState({visible:false})
    };
    openModal=()=>{
        this.setState({visible:true})
    };
    setId=(e)=>{
      this.setState({productId:e});
    };
    render() {
        return (
            <div className={"container"}>
                {
                    this.state.visible?<Modal click={this.closeModal} data={this.state.productId} title={"are you sure to add this product to cart"} name={"add"}/>:null
                }
                {
                    this.state.arr.map((item,index)=><Components key={index} datas={item} open={this.openModal} set={()=>this.setId(item)} color={"lightgray"}/>)
                }

            </div>
        );
    }
}

export default Container;