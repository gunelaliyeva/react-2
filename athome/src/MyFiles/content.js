import React from 'react';
import {Route} from "react-router-dom";
import Container from "./container";
import Cart from "./cart";
import Favorites from "./favorites";

const Content = props => {
    return (
        <div>
            <Route path={"/main"}><Container/></Route>
            <Route path={"/cart"}><Cart/></Route>
            <Route path={"/favorites"}><Favorites/></Route>
        </div>
    );
};

export default Content;