import React from 'react';
import {Link} from "react-router-dom";

const Header = props => {
    return (
        <div className={"header"}>
            <Link to={"/main"} className={"link"}>Home</Link>
            <Link to={"/cart"} className={"link"}>cart</Link>
            <Link to={"/favorites"} className={"link"}>favorites</Link>
        </div>
    );
};

export default Header;