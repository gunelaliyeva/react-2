import React, {Component} from 'react';

class Button2 extends Component {
    state={
        color: this.props.color
    };
    change=()=>{
        if (this.state.color==="lightgray") {
            this.setState({color: "#ff9900"});
            localStorage.setItem(this.props.click.productNumber+"star",this.props.click.name)
        }
        else {
            this.setState({color: "lightgray"});
            localStorage.removeItem(this.props.click.productNumber+"star")
        }
    };
    render() {
        return (
            <div className={this.props.class} onClick={this.change} style={{backgroundColor:this.state.color}}>
                <p>{this.props.name}</p>
            </div>
        );
    }
}

export default Button2;