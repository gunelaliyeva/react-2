import React, {Component} from 'react';
import './style.scss'
import Button from "./commit";
class Modal extends Component {
    render() {
        return (
            <div className={"modal"}>
                <h4>{this.props.title}</h4>
                <div className={"modal-line"}>
                    <Button name={this.props.name} class={"modalButton"} click={this.props.click} inf={this.props.data}/>
                    <Button name={"cancel"} class={"modalButton"} click={this.props.click} inf={this.props.data}/>
                </div>
            </div>
        );
    }
}

export default Modal;
