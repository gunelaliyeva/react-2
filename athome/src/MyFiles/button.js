import React from 'react';

const Button = props => {
    return (
            <div className={props.class} onClick={()=>{props.click()}}>
                <p>{props.name}</p>
            </div>
    );
};

export default Button;